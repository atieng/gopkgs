package plc

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"time"

	"github.com/goburrow/modbus"
)

const (
	Xbase   = 0x0000 // func 02
	Ybase   = 0x2000 // func 01, 05, 15    Y0xx - Y8xx where xx=01-16
	Cbase   = 0x4000 // func 01, 05, 15
	Tbase   = 0xB000 // func 02
	CTbase  = 0xC000 // func 02
	SCbase  = 0xF000 // func 02
	DSbase  = 0x0000 // func 03, 06, 16
	DDbase  = 0x4000 // func 03, 06, 16
	DHbase  = 0x6000 // func 03, 06, 16
	DFbase  = 0x7000 // func 03, 06, 15  -- 0x73e6
	XDbase  = 0xE000 // func 04
	YDbase  = 0xE200 // func 03, 06, 16
	TDbase  = 0xB000 // func 03, 06, 16
	CTDbase = 0xC000 // func 03, 06, 16
	SDbase  = 0xF000 // RO: func 04 RW: 03,06,16 -- 0xF3e7
	TXTbase = 0x9000 // 03,06,16  -- 0x91f3
)

// Function Cheat sheet :
//
// 1    func (mb *client) ReadCoils(address, quantity uint16) (results []byte, err error)
// 2    func (mb *client) ReadDiscreteInputs(address, quantity uint16) (results []byte, err error)
// 3    func (mb *client) ReadHoldingRegisters(address, quantity uint16) (results []byte, err error)
// 4    func (mb *client) ReadInputRegisters(address, quantity uint16) (results []byte, err error)
// 5    func (mb *client) WriteSingleCoil(address, value uint16) (results []byte, err error)
// 6    func (mb *client) WriteSingleRegister(address, value uint16) (results []byte, err error)
// 15   func (mb *client) WriteMultipleCoils(address, quantity uint16, value []byte)
//			(results []byte, err error)
// 16   func (mb *client) WriteMultipleRegisters(address, quantity uint16, value []byte)
//			(results []byte, err error)
// 22   (mb *client) MaskWriteRegister(address, andMask, orMask uint16) (results []byte, err error)
// 23   (mb *client) ReadWriteMultipleRegisters(readAddress, readQuantity, writeAddress,
//			writeQuantity uint16, value []byte) (results []byte, err error)
// 24   (mb *client) ReadFIFOQueue(address uint16) (results []byte, err error)

// clickWordSwap fixes the weird word ordering that the
// get with 32 bit registers.  the two 16bit words are
// reversed so that it is neither big or little endian
func clickWordSwap(v []byte) {
	v[0], v[2] = v[2], v[0]
	v[1], v[3] = v[3], v[1]
}

func (p *PLC) WriteCoil(addr uint16, value int32) (err error) {
	if value != 0 {
		_, err = p.Client.WriteSingleCoil(addr, 0xFF00)
	} else {
		_, err = p.Client.WriteSingleCoil(addr, 0x0000)
	}
	return
}

// ModbusReadUint32registers reads Click PLC 32 bit registers
// which start at 0x4000 for DD1.  Modbus functions 3,6,16.
// Fixup the weird byte ordering that the click generates

func (p *PLC) ReadInt32regs(addr uint16, length uint16) (res []int32, err error) {
	v, err := p.Client.ReadHoldingRegisters(addr, length*2)
	if err != nil {
		return
	}
	res = make([]int32, length)
	buf := bytes.NewReader(v)
	for x := 0; x < int(length); x++ {
		i := x * 4
		clickWordSwap(v[i:])
		err := binary.Read(buf, binary.BigEndian, &res[x])
		if err != nil {
			return nil, err
		}
	}
	return
}

func (p *PLC) ReadFloat32regs(addr uint16, length uint16) (res []float32, err error) {
	v, err := p.Client.ReadHoldingRegisters(addr, length*2)
	if err != nil {
		return
	}
	res = make([]float32, length)
	buf := bytes.NewReader(v)
	for x := 0; x < int(length); x++ {
		i := x * 4
		clickWordSwap(v[i:])
		err := binary.Read(buf, binary.BigEndian, &res[x])
		if err != nil {
			return nil, err
		}
	}
	return
}

func (p *PLC) ReadInt16regs(addr uint16, length uint16) (res []int16, err error) {
	v, err := p.Client.ReadHoldingRegisters(addr, length)
	if err != nil {
		return
	}
	res = make([]int16, length)
	buf := bytes.NewReader(v)
	for x := 0; x < int(length); x++ {
		err := binary.Read(buf, binary.BigEndian, &res[x])
		if err != nil {
			return nil, err
		}
	}
	return
}

type PLC struct {
	handler *modbus.TCPClientHandler
	Client  modbus.Client
	Address string
	//mu sync.Mutex
}

func New(address string) *PLC {
	plc := &PLC{Address: address}
	plc.handler = modbus.NewTCPClientHandler(plc.Address)
	plc.handler.Timeout = 2 * time.Second
	plc.handler.SlaveId = 0xFF
	plc.Client = modbus.NewClient(plc.handler)
	return plc
}

func (plc *PLC) Connect() (err error) {
	err = plc.handler.Connect()
	if err != nil {
		fmt.Printf("modbus connect failed: %v\n", err)
	}
	return
}

func (plc *PLC) Disconnect() {
	plc.handler.Close()
	return
}
