package timer

import (
	"fmt"
	"testing"
)

func TestParse(t *testing.T) {
	ttimer := New()
	fmt.Printf("Hello world of testing!\n")
	ttimer.ParseRange("1-2,4:9-17,22:0")
	ttimer.ParseRange("4:22:30")
	//ttimer.ParseRange("4:22:30-50")
	//ttimer.ParseRange("6:23:0-59")
	ttimer.ParseRange("0:0:0")
	ttimer.ParseRange("0:3,5,7,9:5,7,9")
	ttimer.Print()

	fmt.Printf("IsDayHourMin(2,9,0) = %v\n", ttimer.IsDayHourMin(2, 9, 0))
	fmt.Printf("IsDayHourMin(0,7,0) = %v\n", ttimer.IsDayHourMin(0, 7, 0))

	var d, h, m uint

	for d = 0; d < 7; d++ {
		for h = 0; h < 24; h++ {
			for m = 0; m < 60; m++ {
				x := ttimer.IsDayHourMin(d, h, m)
				if x {
					fmt.Printf("%d:%2.2d:%2.2d\n", d, h, m)
				}
			}
		}
	}

}
