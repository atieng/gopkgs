package flowroutetxt

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
	"regexp"
	"time"
)

type txmsg_resp struct {
	Data   struct{ Id string }
	Errors []struct {
		Id     string
		Status int
		Title  string
	}
}

func Send(from, to, message, url, username, password string) (id string, err error) {
	client := &http.Client{Timeout: time.Second * 4}
	var jsonmsg = []byte(fmt.Sprintf(`{"to":"%s","from":"%s","body":"%s"}`, to, from, message))
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonmsg))
	req.Header.Set("content-type", "application/json; charset=utf-8")
	req.Header.Set("Connection", "close") // do not keep the connection open after POST
	req.SetBasicAuth(username, password)
	resp, err := client.Do(req)
	if err != nil {
		return
	}
	defer resp.Body.Close()

	var r txmsg_resp
	decoder := json.NewDecoder(resp.Body)
	err = decoder.Decode(&r)
	if err != nil {
		return
	}

	if len(r.Errors) != 0 {
		id = r.Errors[0].Id
		err = errors.New(fmt.Sprintf("POST error %d %s", r.Errors[0].Status, r.Errors[0].Title))
		return
	}

	id = r.Data.Id
	return
}

type CallbackFunc func(msg string, arg string)
type Callback struct {
	callback CallbackFunc
	arg      string
}

func (cb Callback) call(msg string) {
	cb.callback(msg, cb.arg)
}

type rxfilter struct {
	to       string
	from     string
	msg      string
	remsg    *regexp.Regexp
	callback *Callback
}

type Receiver struct {
	address string // listen address
	filters []*rxfilter
}

func (rx *Receiver) Process(from, to, msg string) {
	//fmt.Printf("Process %s->%s: %s\n", from, to, msg)
	//fmt.Printf("len(filters) = %d\n", len(rx.filters))
	for _, f := range rx.filters {
		//fmt.Printf("    f=%+v\n", f)
		if f.to != "" && f.to != to {
			continue
		}
		if f.from != "" && f.from != from {
			continue
		}
		if f.msg != "" && !f.remsg.MatchString(msg) {
			continue
		}
		f.callback.call(msg)
		break
	}
	return
}

func (rx *Receiver) AddCallback(from, to, msgregex string, fx CallbackFunc, arg string) (err error) {
	cb := &Callback{callback: fx, arg: arg}
	f := &rxfilter{to: to, from: from, msg: msgregex, callback: cb}
	if msgregex != "" {
		f.remsg, err = regexp.Compile(msgregex)
	}
	//fmt.Printf("Adding filter: %s->%s %s\n", from, to, msgregex)
	rx.filters = append(rx.filters, f)
	//fmt.Printf("len(filters) = %d\n", len(rx.filters))

	return
}

type rxmsg struct {
	Body string
	To   string
	From string
	Id   string
}

func (rx *Receiver) handler(w http.ResponseWriter, r *http.Request) {
	//real_ip := r.Header["X-Real-Ip"]
	//fmt.Printf("Idex: %s %s %s %s %s\n", r.RemoteAddr, r.Method, r.Host, r.URL, real_ip[0])
	decoder := json.NewDecoder(r.Body)
	var msg rxmsg
	err := decoder.Decode(&msg)
	if err != nil {
		return
	}
	//fmt.Printf("r.Body=%+v\n", r.Body)
	//fmt.Printf("r=%+v\n", r)
	defer r.Body.Close()
	//fmt.Printf("t = %+v\n", t)
	rx.Process(msg.From, msg.To, msg.Body)
}

func NewReceiver(address string) (rx *Receiver) {
	rx = &Receiver{address: address}
	http.HandleFunc("/", rx.handler)
	go func() {
		err := http.ListenAndServe(rx.address, nil)
		log.Printf("ListenAndServe %s", err)
	}()
	return
}

/*
func main() {
	var count int = 1
	var username string = "08596072"
	var password string = "538664dcfb2a4bcaa77748a702dd47c4"
	var url string = "https://api.flowroute.com/v2/messages"

	for {
		msg := fmt.Sprintf("hello %d %v", count, time.Now())
		count++
		id, err := Send("14258444384", "14258917957", msg, url, username, password)
		//id, err := SendTxtMessage("12065551212", "14258917957", msg, url, username, password)
		if err != nil {
			fmt.Printf("Send ERROR: %s\n", err)
		} else {
			fmt.Printf("Sent Id: %s\n", id)
		}
		time.Sleep(60 * time.Second)
	}
	//SendText("14258444384", "14258917957", "my stinking message")
	//http.HandleFunc("/", rxhandler)
	//err := http.ListenAndServe("127.0.0.1:8989", nil)
	//if err != nil {
	//	panic("ListenAndServe: " + err.Error())
	//}
	for {
		Send("14258444384", "14258917957", "my stinking message", url, username, password)
		time.Sleep(20 * time.Second)
	}
}
*/
