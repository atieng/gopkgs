package flowroutetxt

import (
	"fmt"
	"testing"
	"time"
)

func TestSend(t *testing.T) {
	username := "08596072"
	password := "538664dcfb2a4bcaa77748a702dd47c4"
	url := "https://api.flowroute.com/v2/messages"
	from := "14258444384"
	to := "14258917957"

	for count := 1; count < 2; count++ {
		msg := fmt.Sprintf("hello %d", count)
		t.Logf("Sending '%s' to %s", msg, to)
		id, err := Send(from, to, msg, url, username, password)
		if err != nil {
			fmt.Printf("Message sent.  id=%s\n", id)
		}
		time.Sleep(1 * time.Second)
	}
}

func msg_callback(msg, arg string) {
	fmt.Printf("msg=[%s] arg=[%s]\n", msg, arg)
}

func msg_hello(msg, arg string) {
	fmt.Printf("Hello Message arg=[%s]\n", arg)
}

func TestRecv(t *testing.T) {
	t.Logf("Starting receiver.  running for 60 seconds")
	txtrx := NewReceiver(":8989")
	txtrx.AddCallback("", "", "hello", msg_hello, "hellodude")
	txtrx.AddCallback("", "", "Hello", msg_hello, "xxxxxxude")
	txtrx.AddCallback("", "", "", msg_callback, "myarg")
	time.Sleep(60 * time.Second)
}
